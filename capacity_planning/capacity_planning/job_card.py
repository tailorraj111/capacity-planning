import frappe
from capacity_planning.capacity_planning.report.manufacturing_capacity_tracking.manufacturing_capacity_tracking import get_capacity

def validate(doc,method):
    check_child_group_capacity(doc)


def check_child_group_capacity(doc):
    job_card_data = frappe.db.sql("""
    select sum(for_quantity) as qty from `tabJob Card` jc where jc.name != %s and jc.status = "Open" and jc.production_item = %s and jc.manufacturing_child_group =%s and jc.posting_date = %s group by posting_date,manufacturing_child_group,jc.production_item
    """,(doc.name,doc.production_item,doc.manufacturing_child_group,doc.posting_date))

    planned = job_card_data[0][0]
    capacity = get_capacity(doc.production_item,doc.manufacturing_child_group)

    if planned > capacity:
        frappe.throw("Production of Item {} on {} by {} is out of capacity".format(doc.production_item,doc.posting_date,doc.manufacturing_child_group))