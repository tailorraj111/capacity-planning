// Copyright (c) 2017, Frappe Technologies Pvt. Ltd. and contributors
// For license information, please see license.txt

frappe.ui.form.on('Production Plan', {
	onload: function(frm){
		
	},

	refresh: function(frm) {
		//Refresh function goes here
		cur_frm.add_custom_button(__("Production Capacity"), function() {
			// frappe.route_options = {"item": row.item_code};
			frappe.set_route("query-report", "Manufacturing Capacity Tracking");
		}); 
		// console.log("in filter")

		frm.fields_dict.po_items.grid.get_field('main_manufacturing_group').get_query = function(frm,cdt,cdn) {
			var row = locals[cdt][cdn]
			return {
				filters: { 
					"item":row.item_code
				},
				query: "capacity_planning.capacity_planning.production_planning.main_manufecture_grp_filter"
			}
		}
		
		frm.fields_dict.po_items.grid.get_field('manufacturing_group').get_query = function(frm,cdt,cdn) {
			var row = locals[cdt][cdn]
			return {
				filters: { 
					"item":row.item_code,
					"parent": row.main_manufacturing_group
				},
				query: "capacity_planning.capacity_planning.production_planning.manufecture_grp_filter"
			}
		}
		

		cur_frm.page.remove_inner_button(__('Work Order / Subcontract PO'),  __('Create'));

		if(frm.doc.docstatus == 1){
			frm.add_custom_button(__('Work Order'), function(){
				// perform desired action such as routing to new form or fetching etc.
				frappe.call({
					"method": "capacity_planning.capacity_planning.custom_doctype.production_plan.production_plan.create_custom_workorder",
					"args":{
						doc: frm.doc
					},
					callback:function(res){
						console.log(res)
						if(res.message == "success"){
							frappe.msgprint("all work orders created successfully")
							window.location.reload();
						}
					}
				});
			  }, __('Create'));
		}
	},

	// get_items: function (frm) {
	// 	frm.clear_table('prod_plan_references');

	// 	frappe.call({
	// 		method: "get_items",
	// 		freeze: true,
	// 		doc: frm.doc,
	// 		callback: function () {
	// 			refresh_field('po_items');
	// 		}
	// 	});
	// },
	consolidate_customer: function (frm) {
		frm.clear_table('prod_plan_references');

		frappe.call({
			"method": "capacity_planning.capacity_planning.custom_doctype.production_plan.production_plan.get_items_consolidate",
			"args":{
				doc: frm.doc
			},
			callback:function(res){
				frm.clear_table('po_items');
				// console.log(res.message)
				$.each(res.message, function(i, d) {
					var row = frappe.model.add_child(cur_frm.doc, "Production Plan Item", "po_items");
					row.item_code = d.item_code;
					row.customer = d.customer;
					row.warehouse = d.warehouse;
					row.pending_qty = d.pending_qty;
					row.planned_qty = 0;
					row.ordered_qty = d.pending_qty;
					row.actual_pending_qty = d.pending_qty;
					row.bom_no = d.bom;
					row.manufacturing_group = d.mg_name;
					row.group_capacity_per_day = d.mg_capacity;
				})
				refresh_field("po_items");
			}
		});
	},
});

frappe.ui.form.on("Production Plan Item", {
	manufacturing_group(frm,cdt,cdn){
		debugger;
	    
	},
	planned_qty: function(frm, cdt, cdn) {
		const row = locals[cdt][cdn];
		row.pending_qty_client = row.pending_qty_client - row.planned_qty;
		row.pending_qty = row.planned_qty
		refresh_field('po_items')
	},
	split: function(frm,cdt,cdn){
		const cur_row = locals[cdt][cdn];

		var row = frappe.model.add_child(cur_frm.doc, "Production Plan Item", "po_items");
					row.item_code = cur_row.item_code;
					row.customer = cur_row.customer;
					row.warehouse = cur_row.warehouse;
					row.pending_qty_client = cur_row.pending_qty_client;
					row.actual_pending_qty = cur_row.pending_qty;
					row.planned_qty = 0;
					row.ordered_qty_client = cur_row.ordered_qty_client;
					row.bom_no = cur_row.bom_no;
					row.past_planned = cur_row.past_planned;
					row.main_manufacturing_group = cur_row.main_manufacturing_group;
					row.manufacturing_item_ref = cur_row.manufacturing_item_ref;
		refresh_field("po_items");
		// frappe.msgprint('btn clicked')
	},
	planned_start_date: function(frm,cdt,cdn){
		const cur_row = locals[cdt][cdn];

		var already_planned = 0;
		frm.doc.po_items.forEach(function(row) { 
			if(cur_row.idx != row.idx){
				if(cur_row.item_code == row.item_code){
					if(cur_row.planned_start_date == row.planned_start_date){
						already_planned += row.planned_qty
					}
				}
			}
		});

		console.log(cur_row)
		console.log(already_planned + ' - ' + cur_row.group_capacity_per_day)

		if(cur_row.group_capacity_per_day <= already_planned){
			frappe.throw('Group Capacity is already filled for date - ' + cur_row.planned_start_date + '. Kindly select another date')
		}

	},
	get_planned_qty(frm,cdt,cdn){
	    var row =locals[cdt][cdn]
	    if(row.item_code && row.manufacturing_group && row.planned_start_date){
	        frappe.call({
	            "method":"capacity_planning.capacity_planning.production_planning.get_planned_qty",
	            "args":{
	                "date":row.planned_start_date,
	                "man_group":row.manufacturing_group,
	                "item_code":row.item_code
	            },
	            "callback":function(res){
	                if(res.message[0][0]){
	                    row.already_planned = res.message[0][0]['qty']
	                    
	                    frm.refresh_field("po_items")
	                }else{
	                    row.already_planned = 0
	                    frm.refresh_field("po_items")
	                }
	                
	            }
	        })
	    }
	    
	},
	get_planned_item_qty(frm,cdt,cdn){
	    var row =locals[cdt][cdn]
	    if(row.item_code){
	        frappe.call({
	            "method":"capacity_planning.capacity_planning.production_planning.get_planned_item_qty",
	            "args":{
	                
	                "item_code":row.item_code
	            },
	            "callback":function(res){
	                console.log("item_qty")
	                if(res.message[0]){
						if(row.past_planned == 0){
							row.past_planned = res.message[0]['qty']
						}
						row.already_planned = 0
	                    row.pending_qty_client = row.pending_qty_client
	                    frm.refresh_field("po_items")
	                }
	               console.log(res)
	            }
	        })
	    } 
	},

	item_code(frm,cdt,cdn){
	    frm.script_manager.trigger("get_planned_qty",cdt,cdn)
	    frm.script_manager.trigger("get_planned_item_qty",cdt,cdn)
	},
	manufacturing_group(frm,cdt,cdn){
	    var row = locals[cdt][cdn]
		if(row.ordered_qty_client == 0){
			row.ordered_qty_client = row.planned_qty
		}
		
	    row.planned_qty = 0
	    
	    frm.script_manager.trigger("get_planned_qty",cdt,cdn)
	    frm.script_manager.trigger("get_planned_item_qty",cdt,cdn)

	    if(row.manufacturing_group){
	        frappe.call({
	            "method":"capacity_planning.capacity_planning.custom_doctype.production_plan.production_plan.get_child_group_capacity",
	            "args":{
	                "item":row.item_code,
	                "grp":row.manufacturing_group
	            },
	            "callback":function(res){
					console.log(res.message, row.item_code, row.manufacturing_group)
	                if(res.message){
	                    row.group_capacity_per_day = res.message
						frappe.db.get_value('BOM', {item: frm.doc.planning_items[item].item}, ['name', 'uom'])
						.then(r => {
							let values = r.message;
							console.log(values.name, values.uom);
							row.bom_no = values.name;
							row.stock_uom = values.uom;
							frm.refresh_field("po_items")
						})
	                }
	            }
	        })
	    }
		frm.refresh_field("po_items")
	},
	planned_start_date(frm,cdt,cdn){
	    frm.script_manager.trigger("get_planned_qty",cdt,cdn)
	}
});

// function check_row(row){
	
// 	if(!row.item_code){
// 		frappe.throw("Fill the Item Code")
// 	}
// 	if(!row.manufacturing_group){
// 		frappe.throw("Fill the Manufacturing Group")
// 	}
// 	if(!row.planned_start_date){
// 		frappe.throw("Fill the Planned Start Date")
// 	}
// 	if(!row.planned_qty){
// 		frappe.throw("Fill the Planned Quantity")
// 	}
	
// }