# -*- coding: utf-8 -*-
# Copyright (c) 2017, Frappe Technologies Pvt. Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe, json, copy
from frappe import msgprint, _
from six import iteritems
import json

from frappe.model.document import Document
from frappe.utils import (flt, cint, nowdate, add_days, comma_and, now_datetime,
	ceil, get_link_to_form, getdate)
from frappe.utils.csvutils import build_csv_response
from erpnext.manufacturing.doctype.bom.bom import validate_bom_no, get_children
from erpnext.manufacturing.doctype.work_order.work_order import get_item_details
from erpnext.setup.doctype.item_group.item_group import get_item_group_defaults



@frappe.whitelist()
def get_child_group_capacity(item,grp):
	capacity = frappe.db.sql("""
	select ic.capacity from `tabManufacturing Group` mg inner join `tabItemwise Capacity` ic on mg.name = ic.parent where mg.is_group = 0 and ic.item = %s and mg.name = %s
	""",(item,grp), as_dict = True)
	if capacity:
		return float(capacity[0].capacity)
	else:
		return 0

@frappe.whitelist()
def get_parent_group_capacity(item,grp):
	capacity = frappe.db.sql("""
	select ic.capacity from `tabManufacturing Group` mg inner join `tabItemwise Capacity` ic on mg.name = ic.parent where mg.is_group = 1 and ic.item = %s and mg.name = %s
	""",(item,grp), as_dict = True)
	if capacity:
		return float(capacity[0].capacity)
	else:
		return 0


@frappe.whitelist()
def get_capacity(item,grp):
	capacity = frappe.db.sql("""
	select ic.capacity from `tabManufacturing Group` mg inner join `tabItemwise Capacity` ic on mg.name = ic.parent where mg.is_group = 1 and ic.item = %s and mg.name = %s
	""",(item,grp))
	# frappe.msgprint(str(item))
	# frappe.msgprint(str(grp))
	# frappe.msgprint(str(capacity))
	return float(capacity[0][0])


@frappe.whitelist()
def get_items_consolidate(doc):
	doc=json.loads(doc)
	if doc["get_items_from"] == "Sales Order":
		# frappe.msgprint(doc["sales_orders"])
		
		try:
			sales_order = doc["sales_orders"]
		except KeyError:
			frappe.throw(_("Please fill the Sales Orders table"), title=_("Sales Orders Required"))

		array_sales_order = []
		for item in sales_order:
			array_sales_order.append(item["sales_order"])
		
		item_condition = ""

		if doc.get("item_code"):
			item_condition = ' and so_item.item_code = {0}'.format(frappe.db.escape(doc["item_code"]))

		items = frappe.db.sql("""
			select distinct so_item.parent, so_item.item_code, so_item.warehouse,
			SUM((so_item.qty - so_item.work_order_qty) * so_item.conversion_factor) as pending_qty, so_item.description, so_item.name,
			(select `tabSales Order`.customer from `tabSales Order` where so_item.parent = `tabSales Order`.name) as customer,
			(select name from `tabBOM` where `tabBOM`.item = so_item.item_code and `tabBOM`.is_active = 1) as bom,
			mg.name as mg_name, mg.capacity as mg_capacity
			from `tabSales Order Item` so_item
			left join `tabManufacturing Group` mg on mg.item = so_item.item_code
			where so_item.parent in (%s)
			and so_item.docstatus = 1 and so_item.qty > so_item.work_order_qty
			and exists (select name from `tabBOM` bom where bom.item=so_item.item_code
					and bom.is_active = 1) %s
			group by customer, item_code
			order by customer	
				""" % \
			(", ".join(["%s"] * len(array_sales_order)), item_condition), tuple(array_sales_order), as_dict=1)

		return items
		
		# for item in items:
		# 	frappe.msgprint(item.item_code + " - " + str(item.pending_qty) + " - " + item.customer)
		
@frappe.whitelist()
def create_custom_workorder(doc):
	doc=json.loads(doc)
	planned_items = doc["po_items"]
	for item in planned_items:
		# frappe.msgprint(item["item_code"] + ' - ' + item["customer"])
		order_reference = item.get("work_order_reference", 'notfound')
		if order_reference == "notfound":
			work_order = frappe.new_doc("Work Order")
			work_order.company = doc["company"]
			work_order.production_item = item["item_code"]
			work_order.qty = item["planned_qty"]
			work_order.planned_start_date = item["planned_start_date"]
			work_order.bom_no = item["bom_no"]

			work_order.description = item["item_code"]
			work_order.stock_uom = item["stock_uom"]
			work_order.manufacturing_group = item["manufacturing_group"]
			work_order.production_plan = item["parent"]
			work_order.production_plan_item = item["name"]

			work_order.save()

			frappe.db.set_value('Production Plan Item', item["name"], 'work_order_reference', work_order.name)
	
	return "success"

			