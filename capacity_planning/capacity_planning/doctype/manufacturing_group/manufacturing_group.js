// Copyright (c) 2021, raaj@akhilaminc.com and contributors
// For license information, please see license.txt

frappe.ui.form.on('Manufacturing Group', {
	validate: function(frm) {
		var def =0
		frm.doc.itemwise_capacity.forEach(element => {
			if(element.default == 1 && def == 0){
				def = 1
			}else if(element.default == 1 && def == 1){
				frappe.throw("Only 1 item can be default.")
			}
			// else {
			// 	frappe.throw("Set at least 1 default Item.")
			// }
		});
		// for item in self.itemwise_capacity:
		// 	if item.default == 1 and default == 0:
		// 		default = 1
		// 	elif item.default == 1 and default == 1:
		// 		frappe.throw("Only 1 item can be default.")
		// 	else:
		// 		frappe.throw("Set at least 1 default Item.")
	}
});

frappe.ui.form.on('Itemwise Capacity', {
	default(frm,cdt,cdn){
	    let row = locals[cdt][cdn]
	    if(row.default == 1){
	       frm.set_value("item",row.item)
	       frm.set_value("capacity",row.capacity)
	       frm.refresh_field("item","capacity")
	    }
	}
});
