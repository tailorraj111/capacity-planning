# Copyright (c) 2021, raaj@akhilaminc.com and contributors
# For license information, please see license.txt

import frappe
from frappe import _
import json
from frappe.model.document import Document
from frappe.utils import nowdate

class ManufacturingPlan(Document):
	@frappe.whitelist()
	def make_material_request(self):
		pass
		mr = frappe.new_doc("Material Request")
		mr.tansaction_date = nowdate()
		mr.status = "Draft"
		mr.company = self.company
		mr.material_request_type = "Purchase"

		
		for item in self.mr_items:
			# frappe.msgprint(item["item_code"])
			mr.append("items", {
				"item_code": item.item_code,
				"from_warehouse": item.from_warehouse,
				"qty": item.quantity,
				"schedule_date": nowdate(),
				"warehouse": item.warehouse,
				'material_request_plan_item': item.name
			})

		# frappe.msgprint(po.name)
		mr.save()
		
		frappe.msgprint("Material Request: " + mr.name + " Created Successfully!")

	@frappe.whitelist()
	def get_open_sales_orders(self):
		""" Pull sales orders  which are pending to deliver based on criteria selected"""
		open_so = get_sales_orders(self)

		if open_so:
			self.add_so_in_table(open_so)
		else:
			frappe.msgprint(_("Sales orders are not available for production"))

	def add_so_in_table(self, open_so):
		""" Add sales orders in the table"""
		self.set('sales_orders', [])

		for data in open_so:
			self.append('sales_orders', {
				'sales_order': data.name,
				'sales_order_date': data.transaction_date,
				'delivery_date': data.delivery_date,
				'customer': data.customer,
				'grand_total': data.base_grand_total
			})



	
def get_sales_orders(self):
	so_filter = item_filter = ""
	if self.from_date:
		so_filter += " and so.transaction_date >= %(from_date)s"
	if self.to_date:
		so_filter += " and so.transaction_date <= %(to_date)s"
	# if self.customer:
	# 	so_filter += " and so.customer = %(customer)s"
	# if self.project:
	# 	so_filter += " and so.project = %(project)s"
	# if self.sales_order_status:
	# 	so_filter += "and so.status = %(sales_order_status)s"

	# if self.item_code:
	# 	item_filter += " and so_item.item_code = %(item)s"

	open_so = frappe.db.sql("""
		select distinct so.name, so.transaction_date, so.customer, so.base_grand_total, so.delivery_date
		from `tabSales Order` so, `tabSales Order Item` so_item
		where so_item.parent = so.name
			and so.docstatus = 1 and so.status not in ("Stopped", "Closed")
			and so.company = %(company)s
			and so_item.qty > so_item.work_order_qty {0} {1}
			and (exists (select name from `tabBOM` bom where bom.item=so_item.item_code
					and bom.is_active = 1)
				or exists (select name from `tabPacked Item` pi
					where pi.parent = so.name and pi.parent_item = so_item.item_code
						and exists (select name from `tabBOM` bom where bom.item=pi.item_code
							and bom.is_active = 1)))
		""".format(so_filter, item_filter), {
			"from_date": self.from_date,
			"to_date": self.to_date,
			# "customer": self.customer,
			# "project": self.project,
			# "item": self.item_code,
			"company": self.company,
			# "sales_order_status": self.sales_order_status
		}, as_dict=1)
	return open_so

@frappe.whitelist()
def get_items_consolidate(doc):
	doc=json.loads(doc)

	try:
		sales_order = doc["sales_orders"]
	except KeyError:
		frappe.throw(_("Please fill the Sales Orders table"), title=_("Sales Orders Required"))

	array_sales_order = []
	for item in sales_order:
		array_sales_order.append(item["sales_order"])
	
	item_condition = ""

	if doc.get("item_code"):
		item_condition = ' and so_item.item_code = {0}'.format(frappe.db.escape(doc["item_code"]))

	items = frappe.db.sql("""
		select distinct so_item.parent, so_item.item_code, so_item.warehouse,
		SUM((so_item.qty - so_item.work_order_qty) * so_item.conversion_factor) as pending_qty, so_item.description, so_item.name,
		(select `tabSales Order`.customer from `tabSales Order` where so_item.parent = `tabSales Order`.name) as customer,
		(select name from `tabBOM` where `tabBOM`.item = so_item.item_code and `tabBOM`.is_active = 1) as bom,
		mg.name as mg_name, mg.capacity as mg_capacity
		from `tabSales Order Item` so_item
		left join `tabManufacturing Group` mg on mg.item = so_item.item_code
		where so_item.parent in (%s)
		and so_item.docstatus = 1 and so_item.qty > so_item.work_order_qty
		and exists (select name from `tabBOM` bom where bom.item=so_item.item_code
				and bom.is_active = 1) %s
		group by item_code
		order by item_code	
			""" % \
		(", ".join(["%s"] * len(array_sales_order)), item_condition), tuple(array_sales_order), as_dict=1)

	return items
	
	# for item in items:
	# 	frappe.msgprint(item.item_code + " - " + str(item.pending_qty) + " - " + item.customer)


@frappe.whitelist()
def create_production_plan(doc):
	doc=json.loads(doc)
	production_plan = frappe.new_doc("Production Plan")
	for item in doc["po_items"]:
		production_plan.append('po_items', {
			'item_code': item["item"],
			'main_manifacturing_group': item["manufacturing_group"],
			'planned_qty': item["planned_qty"]
		})

	return production_plan.name

@frappe.whitelist()
def get_map_doc(source_name, target_doc=None):
	
	# get_list = frappe.db.get_list('Production Plan', filters={'manufacturing_plan_id': source_name})

	from frappe.model.mapper import get_mapped_doc

	def set_missing_values(source, target):
		pass
    
	def update_item(obj, target, source_parent):
		target.bom_no = obj.bom_no
		target.planned_qty = obj.planned_qty
		target.pending_qty_client = obj.planned_qty
		target.warehouse = frappe.db.get_single_value('Stock Settings', 'default_warehouse')
		target.past_planned = 0

	doc = get_mapped_doc("Manufacturing Plan", source_name,   {
		"Manufacturing Plan": {
			"doctype": "Production Plan",
			"field_map": {
            }
        },
		"Manufacturing Plan Item": {
			"doctype": "Production Plan Item",
			"field_no_map": ["manufacturing_group"],
			"field_map": {
				"item_code":"item_code",
				"planned_qty": "ordered_qty_client",
				"manufacturing_group": "main_manufacturing_group"
            },
			"validation": {
				"planned_qty": [">", 0]
			},
			"postprocess": update_item
			
		}
	}, target_doc, set_missing_values)
	# temp = ''

	# if len(get_list) > 1:
	# 	where_clause = ''
	# 	for item in get_list:
	# 		if where_clause != '':
	# 			where_clause = "where pp.name in [" + item.name
	# 		else:
	# 			where_clause += ", " + item.name
		
	# 	where_clause += "]"
	# else:
	# 	where_clause = "where pp.name = " + get_list[0].name

	# get_item_list = frappe.db.sql("""
	# 	select
	# 	ppi.name, ppi.item_code, sum(ppi.planned_qty) as planned_qty, ppi.ordered_qty_client
	# 	from `tabProduction Plan Item` ppi
	# 	left join `tabProduction Plan` pp on pp.name = ppi.parent
	# 	%s
	# 	group by ppi.item_code
	# """% (where_clause), as_dict= True)

	# for item in doc.po_items:
	# 	for x in get_item_list:
	# 		if item.item_code == x.item_code:
	# 			remainder = item.planned_qty - doc.planned_item
	# 			if remainder > 0:
	# 				item.planned_qty = remainder
	# 			else:
	# 				item.
			
	# for item in get_list:
	# 	temp += item.name + ', '



		

	# frappe.msgprint(temp + ' - Length: ' + str(len(get_list)))
	return doc