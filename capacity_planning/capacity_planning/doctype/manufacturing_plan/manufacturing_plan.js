// Copyright (c) 2021, raaj@akhilaminc.com and contributors
// For license information, please see license.txt

frappe.ui.form.on('Manufacturing Plan','posting_date',function(frm) {
    frm.set_value("posting_date",frappe.datetime.add_days(frappe.datetime.nowdate(), frm.doc.posting_date));
});

frappe.ui.form.on('Manufacturing Plan Item','before_save', function(frm) {
		// your code here
		var total_production = (flt(cur_frm.doc.planned_quantity)-(flt(cur_frm.doc.daily_capacity)));
    cur_frm.set_value('no_of_days_required',total_production);
	
});

frappe.ui.form.on('Manufacturing Plan', {
	refresh: function(frm) {
		frm.fields_dict.po_items.grid.get_field('manufacturing_group').get_query = function(frm,cdt,cdn) {
			var row = locals[cdt][cdn]
			return {
				filters: { 
					"item":row.item_code
				},
				query: "capacity_planning.capacity_planning.production_planning.main_manufecture_grp_filter"
			}
		}

		if(frm.doc.docstatus == 1){
			frm.add_custom_button(__('Material Request'), function(){

				frappe.call({
					method: "make_material_request",
					freeze: true,
					doc: frm.doc,
					callback: function(r) {
						frm.reload_doc();
					}
				});
				
			}, __('Create'));

			frm.add_custom_button(__('Production Plan'), function(){
				// perform desired action such as routing to new form or fetching etc.
				// frappe.call({
				// 	"method": "capacity_planning.capacity_planning.doctype.manufacturing_plan.manufacturing_plan.create_production_plan",
				// 	"args":{
				// 		doc: frm.doc
				// 	},
				// 	callback:function(res){
				// 		console.log(res)
				// 		frappe.set_route("Form", "Production Plan", res.message.name);
				// 	}
				// });

				frappe.model.open_mapped_doc({
					method: "capacity_planning.capacity_planning.doctype.manufacturing_plan.manufacturing_plan.get_map_doc",
					frm: cur_frm
				})


				// var production_plan = frappe.model.get_new_doc('Production Plan');
				// production_plan.company = frm.doc.company
				// production_plan.posting_date = new Date()
				// for(var item in frm.doc.po_items){
				// 	var row = frappe.model.add_child(production_plan, "Production Plan Item", "po_items");
				// 	row.item_code = frm.doc.po_items[item].item;
				// 	row.main_manufacturing_group = frm.doc.po_items[item].manufacturing_group;
				// 	row.planned_qty = frm.doc.po_items[item].quantity;
				// 	// var bom_no = "";
				// 	frappe.db.get_value('BOM', {item: frm.doc.po_items[item].item}, 'name')
				// 		.then(r => {
				// 			let values = r.message;
				// 			console.log(values.name);
				// 			row.bom_no = values.name;
							
				// 		})

				// 	row.ordered_qty_client = frm.doc.po_items[item].quantity;
				// 	row.pending_qty_client = frm.doc.po_items[item].quantity;
				// 	row.past_planned = 0
				// 	row.warehouse = "Finished Goods - A";
				// 	// row.bom_no = bom_no;
				// 	refresh_field("po_items")
				// }
				

				// frappe.set_route('Form', 'Production Plan', production_plan.name);
				
			}, __('Create'));
		}
	},
	get_sales_orders: function(frm) {
		frappe.call({
			method: "get_open_sales_orders",
			doc: frm.doc,
			callback: function(r) {
				refresh_field("sales_orders");
			}
		});
	},
	get_items: function (frm) {
		frappe.call({
			"method": "capacity_planning.capacity_planning.doctype.manufacturing_plan.manufacturing_plan.get_items_consolidate",
			"args":{
				doc: frm.doc
			},
			callback:function(res){
				frm.clear_table('po_items');
				// console.log(res.message)
				$.each(res.message, function(i, d) {
					var row = frappe.model.add_child(cur_frm.doc, "Manufacturing Plan Item", "po_items");
					row.item_code = d.item_code;
					row.customer = d.customer;
					// row.warehouse = d.warehouse;
					row.quantity = d.pending_qty;
					row.planned_qty = 0;
					// row.ordered_qty = d.pending_qty;
					row.bom_no = d.bom;
					// row.manufacturing_group = d.mg_name;
					// row.daily_capacity = d.mg_capacity;
				})
				refresh_field("po_items");
			}
		});
	},
	get_items_for_mr: function(frm) {
		if (!frm.doc.for_warehouse) {
			frappe.throw(__("Select warehouse for material requests"));
		}

		if (frm.doc.ignore_existing_ordered_qty) {
			frm.events.get_items_for_material_requests(frm);
		} else {
			const title = __("Transfer Materials For Warehouse {0}", [frm.doc.for_warehouse]);
			var dialog = new frappe.ui.Dialog({
				title: title,
				fields: [
					{
						"fieldtype": "Table MultiSelect", "label": __("Source Warehouses (Optional)"),
						"fieldname": "warehouses", "options": "Production Plan Material Request Warehouse",
						"description": __("System will pickup the materials from the selected warehouses. If not specified, system will create material request for purchase."),
						get_query: function () {
							return {
								filters: {
									company: frm.doc.company
								}
							};
						},
					},
				]
			});

			dialog.show();

			dialog.set_primary_action(__("Get Items"), () => {
				let warehouses = dialog.get_values().warehouses;
				frm.events.get_items_for_material_requests(frm, warehouses);
				dialog.hide();
			});
		}
	},

	get_items_for_material_requests: function(frm, warehouses) {
		const set_fields = ['actual_qty', 'item_code','item_name', 'description', 'uom', 'from_warehouse',
			'min_order_qty', 'required_bom_qty', 'quantity', 'sales_order', 'warehouse', 'projected_qty', 'ordered_qty',
			'reserved_qty_for_production', 'material_request_type'];

		frappe.call({
			method: "erpnext.manufacturing.doctype.production_plan.production_plan.get_items_for_material_requests",
			freeze: true,
			args: {
				doc: frm.doc,
				warehouses: warehouses || []
			},
			callback: function(r) {
				if(r.message) {
					frm.set_value('mr_items', []);
					$.each(r.message, function(i, d) {
						var item = frm.add_child('mr_items');
						for (let key in d) {
							if (d[key] && in_list(set_fields, key)) {
								item[key] = d[key];
							}
						}
					});
				}
				refresh_field('mr_items');
			}
		});
	},
});

frappe.ui.form.on('Manufacturing Plan Item', {
	refresh(frm) {
		// your code here
	},
	manufacturing_group(frm,cdt,cdn){
	    var row = locals[cdt][cdn]

	    if(row.manufacturing_group){
	        frappe.call({
	            "method":"capacity_planning.capacity_planning.custom_doctype.production_plan.production_plan.get_parent_group_capacity",
	            "args":{
	                "item":row.item_code,
	                "grp":row.manufacturing_group
	            },
	            "callback":function(res){
					console.log(res.message, row.item, row.manufacturing_group)
	                if(res.message){
	                    row.daily_capacity = res.message
						row.no_of_days_required = Math.round(row.planned_qty/res.message)
	                    frm.refresh_field("po_items")
	                }
	            }
	        })
	    }
	},
	planned_qty(frm,cdt,cdn){
	    var row = locals[cdt][cdn]
		let capacity = 0
		if(row.daily_capacity){
			capacity = row.daily_capacity;
			row.no_of_days_required = Math.round(row.planned_qty/capacity)
		}
		else{
			row.no_of_days_required = 0
		}
		
		frm.refresh_field("po_items")
	},
})
