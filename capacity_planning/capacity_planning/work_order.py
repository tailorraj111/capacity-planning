import frappe

def set_manufecture_grp(doc,method):
    if not doc.manufacturing_group and doc.production_plan_item:
        doc.manufacturing_group = frappe.db.get_value("Production Plan Item",doc.production_plan_item,"manufacturing_group")

def set_manufecture_grp_jobcard(doc,method):
    if not doc.manufacturing_parent_group and doc.work_order:
        doc.manufacturing_parent_group = frappe.db.get_value("Work Order",doc.work_order,"manufacturing_group")
