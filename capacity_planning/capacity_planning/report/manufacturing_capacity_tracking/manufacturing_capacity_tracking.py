# Copyright (c) 2013, raaj@akhilaminc.com and contributors
# For license information, please see license.txt

import frappe
from frappe import _
# from console import console

def execute(filters=None):
	columns = get_columns(filters)
	data = []
	if filters.get("type") == "Parent Group":
		data = get_data(filters)
	elif filters.get("type") == "Child Group":
		data = child_data(filters)
	return columns, data


def child_data(filters):
	data = []
	job_card_data = frappe.db.sql("""
	select posting_date,production_item as item,manufacturing_child_group,sum(for_quantity) as qty from `tabJob Card` where status in ("Open","Work In Progress")  group by posting_date,manufacturing_child_group,production_item
	""",as_dict = 1)
	# frappe.msgprint(str(job_card_data))
	for item in job_card_data:
		# frappe.msgprint(str(item.item))
		# frappe.msgprint(str(item.manufacturing_child_group))
		capacity = get_child_capacity(item.item,item.manufacturing_child_group)
		data.append({
			"date":item.posting_date,
			"item":item.item,
			"man_grp":item.manufacturing_child_group,
			"planned_qty":item.qty,
			"cap":capacity,
			"avl_qty":capacity - float(item.qty)
		})

	return data

def get_data(filters):
	data = []
	planned_data = frappe.db.sql("""
	select poi.item_code as item,poi.manufacturing_group as man_grp,DATE(poi.planned_start_date) as date,sum(poi.planned_qty) as qty from `tabProduction Plan Item` poi inner join `tabProduction Plan` pp on poi.parent = pp.name where pp.docstatus = 1 and pp.status != "Closed" group by DATE(poi.planned_start_date),poi.manufacturing_group,poi.item_code
	""",as_dict = 1)
	# frappe.msgprint(str(planned_data))
	for item in planned_data:
		capacity = get_capacity(item.item,item.man_grp)
		data.append({
			"date":item.date,
			"item":item.item,
			"man_grp":item.man_grp,
			"planned_qty":item.qty,
			"cap":capacity,
			"avl_qty":capacity - float(item.qty)
		})

	return data



	# console(job_card_data).log()

@frappe.whitelist()
def get_capacity(item,grp):
	capacity = frappe.db.sql("""
	select ic.capacity from `tabManufacturing Group` mg inner join `tabItemwise Capacity` ic on mg.name = ic.parent where mg.is_group = 1 and ic.item = %s and mg.name = %s
	""",(item,grp))
	# frappe.msgprint(str(item))
	# frappe.msgprint(str(grp))
	# frappe.msgprint(str(capacity))
	return float(capacity[0][0])

@frappe.whitelist()
def get_child_capacity(item,grp):
	capacity = frappe.db.sql("""
	select ic.capacity from `tabManufacturing Group` mg inner join `tabItemwise Capacity` ic on mg.name = ic.parent where mg.is_group = 0 and ic.item = %s and mg.name = %s
	""",(item,grp))
	# frappe.msgprint(str(item))
	# frappe.msgprint(str(grp))
	# frappe.msgprint(str(capacity))
	return float(capacity[0][0])

def get_columns(filters):
	
	
	columns = [
		{
			"label":_("Date"),
			"fieldname": "date",
			"fieldtype": "date",
			"width": 150
		},
		{
			"label":_("Item"),
			"fieldname": "item",
			"fieldtype": "link",
			"options":"Item",
			"width": 90
		},
		{
			"label":_("Manufacturing Group"),
			"fieldname": "man_grp",
			"fieldtype": "link",
			"options":"Manufacturing Group",
			"width": 90
		},
		{
			"label":_("Planned Qty"),
			"fieldtype":"Float",
			"fieldname": "planned_qty",
			"width": 90
		},
		{
			"label":_("Capacity"),
			"fieldtype":"Float",
			"fieldname": "cap",
			"width": 90
		},
		{
			"label":_("Available Qty"),
			"fieldtype":"Float",
			"fieldname": "avl_qty",
			"width": 90
		}
		
		
		
	]

	return columns
