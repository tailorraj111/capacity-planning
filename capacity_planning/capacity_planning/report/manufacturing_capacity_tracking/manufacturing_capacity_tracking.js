// Copyright (c) 2016, raaj@akhilaminc.com and contributors
// For license information, please see license.txt
/* eslint-disable */

frappe.query_reports["Manufacturing Capacity Tracking"] = {
	"filters": [
		{
			"fieldname":"type",
			"label": __("Report For"),
			"fieldtype": "Select",
			"options": ["Parent Group","Child Group"],
			"width": "60px"
		},

	]
};
