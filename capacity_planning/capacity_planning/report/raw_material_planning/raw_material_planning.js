// Copyright (c) 2016, raaj@akhilaminc.com and contributors
// For license information, please see license.txt
/* eslint-disable */

frappe.query_reports["Raw Material Planning"] = {
	"filters": [
		{
			"fieldname":"item_name",
			"label": __("Item Name"),
			"fieldtype": "Link",
			"width": "80",
			"options": "Item"
		},
		{
			"fieldname":"item_group",
			"label": __("Item Group"),
			"fieldtype": "Link",
			"width": "80",
			"options": "Item Group"
		},
		{
			"fieldname":"warehouse",
			"label": __("Warehouse"),
			"fieldtype": "Link",
			"width": "80",
			"options": "Warehouse"
		},
	]
};
