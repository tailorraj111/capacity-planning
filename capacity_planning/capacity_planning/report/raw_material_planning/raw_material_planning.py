# Copyright (c) 2013, raaj@akhilaminc.com and contributors
# For license information, please see license.txt

import frappe
from frappe import _

def execute(filters=None):
	columns, data  = get_columns(filters), get_data(filters)
	return columns, data

def get_data(filters=None):
	condition = ""
	if filters.item_name:
		condition += " and POI.item_code = \"" + filters.get("item_name") + "\""
	
	if filters.item_group:
		condition += " and POI.item_group = \"" + filters.get("item_group") + "\""
	
	if filters.warehouse:
		condition += " and POI.warehouse = \"" + filters.get("warehouse") + "\""
		
	data = frappe.db.sql("""
	select
    POI.item_code,
    POI.item_name,
    POI.stock_UOM,
    POI.item_group,
    POI.warehouse,
    IF(reorder.warehouse_reorder_level is null, 0, warehouse_reorder_level) as qty_maintain,
    (select qty_after_transaction from `tabStock Ledger Entry` SLE where SLE.item_code = POI.item_code and SLE.warehouse = POI.warehouse order by SLE.creation desc limit 1) as cur_stock,
    (select qty_after_transaction from `tabStock Ledger Entry` SLE where SLE.item_code = POI.item_code and SLE.warehouse = POI.warehouse order by SLE.creation desc limit 1) - IF(reorder.warehouse_reorder_level is null, 0, warehouse_reorder_level) as variance,
    IF((select sum(actual_qty) from `tabStock Ledger Entry` SLE where SLE.warehouse = "Work In Progress - A" and SLE.item_code = POI.item_code)  is null, 0, (select sum(actual_qty) from `tabStock Ledger Entry` SLE where SLE.warehouse = "Work In Progress - A" and SLE.item_code = POI.item_code)) as workorder_reserve,
    GROUP_CONCAT(PO.name) as pur_order,
	sum(POI.qty) as purchase_qty,
    PO.schedule_date as exp_date,
	0.00 as order_quantity
    from
    `tabPurchase Order Item` POI
    left join `tabPurchase Order` PO on PO.name = POI.parent
    right join `tabItem` item on item.name = POI.item_code
    left join `tabItem Reorder` reorder on reorder.parent = item.name and reorder.warehouse = POI.warehouse
	where PO.status = "To Receive" or PO.status = "To Receive and Bill"
	%s
	group by POI.item_code, POI.item_group, POI.warehouse
    order by POI.item_code
	""" % (condition) ,as_dict = 1)

	for item in data:
		order_quantity = item.qty_maintain - (item.cur_stock + item.purchase_qty - item.workorder_reserve)

		item.order_quantity = order_quantity


	return data



def get_columns(filters):
	columns = [
		{
			"label":_("Item Code"),
			"fieldname": "item_code",
			"fieldtype": "data",
			"width": 90
		},
		{
			"label":_("Item Node"),
			"fieldname": "item_name",
			"fieldtype": "data",
			"width": 90
		},
		{
			"label":_("UOM"),
			"fieldname": "stock_UOM",
			"fieldtype": "data",
			"width": 90
		},
		{
			"label":_("Item Group"),
			"fieldname": "item_group",
			"fieldtype": "data",
			"width": 90
		},
		{
			"label":_("Warehouse"),
			"fieldname": "warehouse",
			"fieldtype": "data",
			"width": 90
		},
		{
			"label":_("Minumum qty to maintain"),
			"fieldname": "qty_maintain",
			"fieldtype": "float",
			"width": 90
		},
		{
			"label":_("Current Stock"),
			"fieldname": "cur_stock",
			"fieldtype": "float",
			"width": 90
		},
		{
			"label":_("Booked Stock"),
			"fieldname": "workorder_reserve",
			"fieldtype": "float	",
			"width": 90
		},
		{
			"label":_("Qty variance"),
			"fieldname": "variance",
			"fieldtype": "float	",
			"width": 90
		},
		{
			"label":_("Purchase Order"),
			"fieldname": "pur_order",
			"fieldtype": "float	",
			"width": 90
		},
		{
			"label":_("Purchase Qty"),
			"fieldname": "purchase_qty",
			"fieldtype": "float	",
			"width": 90
		},
		{
			"label":_("Expected Date"),
			"fieldname": "exp_date",
			"fieldtype": "date",
			"width": 90
		},
		{
			"label":_("qty to place order"),
			"fieldname": "order_quantity",
			"fieldtype": "float",
			"width": 90
		},
	]
	return columns
