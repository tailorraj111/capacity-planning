// Copyright (c) 2016, raaj@akhilaminc.com and contributors
// For license information, please see license.txt
/* eslint-disable */

frappe.query_reports["Raw Material Request Custom"] = {
	"filters": [
		{
			"fieldname":"company",
			"label": __("Company"),
			"fieldtype": "Link",
			"options": "Company",
			"default": frappe.defaults.get_user_default("Company")
		},
		{
			"fieldname":"warehouse",
			"label": __("Warehouse"),
			"fieldtype": "Link",
			"options": "Warehouse",
			"get_query": () => {
				return {
					filters: {
						company: frappe.query_report.get_filter_value('company')
					}
				}
			}
		},
		{
			"fieldname":"item_code",
			"label": __("Item"),
			"fieldtype": "Link",
			"options": "Item",
			"get_query": function() {
				return {
					query: "erpnext.controllers.queries.item_query"
				}
			}
		},
		{
			"fieldname":"item_group",
			"label": __("Item Group"),
			"fieldtype": "Link",
			"options": "Item Group"
		},
		{
			"fieldname":"brand",
			"label": __("Brand"),
			"fieldtype": "Link",
			"options": "Brand"
		},
		{
			"fieldname":"include_uom",
			"label": __("Include UOM"),
			"fieldtype": "Link",
			"options": "UOM"
		}
	],
	onload: function(report) {
		report.page.add_inner_button(__("Make Material Request"), function() {
			// frappe.msgprint("Test");
			console.log(report)
			let checked_rows_indexes = report.datatable.rowmanager.getCheckedRows();
			let checked_rows = checked_rows_indexes.map(i => report.data[i]);
			console.log(checked_rows)
			for(var item in checked_rows){
				console.log(checked_rows[item].item_code)
			}
			frappe.call({
				"method":"capacity_planning.capacity_planning.report.raw_material_request_custom.raw_material_request_custom.insert_material_request",
				"args":{
					"data":checked_rows
				},
				"callback":function(res){
					console.log(res)
					if(res.message){
						frappe.msgprint("Material request is created successfully")
					}
					
					// frappe.set_route('Form', 'Purchase Order', res.message);
				}
			})
			// debugger;
			// var doc = frappe.model.get_new_doc('Purchase Order');
			// doc.transaction_date = new Date();
			// $.each(checked_rows, function(i, item) {
			// 	var item_row = doc.add_child("items")
			// 	// console.log(item)
			// 	item_row.item_code = item.item_code;
			// 	item_row.item_name = item.item_name;
			// 	item_row.required_by = new Date();
			// 	item.qty = item.shortage_qty;
			// 	frappe.set_route('Form', 'Purchase Order', doc.name);
			// });
		});

		// report.page.add_action_item(__("Test"), function() {
		// 	let checked_rows_indexes = report.datatable.rowmanager.getCheckedRows();
		// 	let checked_rows = checked_rows_indexes.map(i => report.data[i]);
		// 	/* DO WHATEVER YOU LIKE WITH YOUR `checked_rows`!  */
        //     // debugger;
		// });
	},
	get_datatable_options(options) {
		return Object.assign(options, {
			checkboxColumn: true,
		});
	},
};
