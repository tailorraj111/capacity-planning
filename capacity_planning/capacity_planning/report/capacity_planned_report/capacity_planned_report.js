// Copyright (c) 2016, raaj@akhilaminc.com and contributors
// For license information, please see license.txt
/* eslint-disable */

frappe.query_reports["Capacity Planned Report"] = {
	"filters": [
		{
			"fieldname":"report_type",
			"label": __("Report Type"),
			"fieldtype": "Select",
			"width": "80",
			"options": ["Customer Group", "Customer Territory", "Customer Name"],
			"default": "Customer Group"
		},
		{
			"fieldname":"from_date",
			"label": __("From Date"),
			"fieldtype": "Date",
			"width": "80",
			"default": frappe.datetime.nowdate()
		},
		{
			"fieldname":"to_date",
			"label": __("To Date"),
			"fieldtype": "Date",
			"width": "80",
			"default": frappe.datetime.nowdate()
		},
	]
};
