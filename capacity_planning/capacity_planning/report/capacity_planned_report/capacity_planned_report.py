# Copyright (c) 2013, raaj@akhilaminc.com and contributors
# For license information, please see license.txt

import frappe
from frappe import _
import pandas as pd
import numpy as np
# from console import console
from frappe.utils import getdate

def execute(filters=None):
	if filters.get("report_type") == "Customer Name":
		return get_customerwise_data(filters)
	if filters.get("report_type") == "Customer Group":
		return get_customer_groupwise_data(filters)
	if filters.get("report_type") == "Customer Territory":
		return get_customer_territorypwise_data(filters)
	

def get_customerwise_data(filters=None):
	from_date = getdate(filters.get("from_date"))
	to_date = getdate(filters.get("to_date"))

	value = {"from_date": str(from_date), "to_date": str(to_date)}

	# frappe.msgprint(str(from_date) + ' - ' + str(to_date))
	data_sql = frappe.db.sql("""
	select items.customer, items.item_code, SUM(items.planned_qty) as qty, items.creation from `tabProduction Plan Item` items where items.customer != "" and items.creation BETWEEN %(from_date)s AND ADDDATE(%(to_date)s, 1) GROUP BY items.customer, items.item_code
	""", values = value , as_dict = 1)
	# data = [{'customer': 'Test', 'item_code': 'Item A', 'qty': 1.0}, {'customer': 'Test 1', 'item_code': 'Item B', 'qty': 1.0}, {'customer': 'Test 2', 'item_code': 'Item A', 'qty': 1.0}, {'customer': 'Test 3', 'item_code': 'Item A', 'qty': 1.0}, {'customer': 'Test 4', 'item_code': 'Item B', 'qty': 1.0}, {'customer': 'Test 5', 'item_code': 'Item B', 'qty': 1.0}, {'customer': 'Test 6', 'item_code': 'Item B', 'qty': 1.0}, {'customer': 'Test 7', 'item_code': 'Item B', 'qty': 1.0}, {'customer': 'Test 8', 'item_code': 'Item A', 'qty': 1.0}, {'customer': 'Test 9', 'item_code': 'Item A', 'qty': 12.0}, {'customer': 'Test 9', 'item_code': 'Item B', 'qty': 10.0}]
	
	flag = True
	data = []
	for d in data_sql:
		d_n = dict(d)
		data.append(d_n)
		# console(type(d)).log()

	if data == []:
		d = {"customer": "", "item_code": "", "qty": 0, "creation": ""}
		data.append(d)
		flag = False

	df = pd.DataFrame.from_dict(data)
	# df_t = df.transpose()
	res = df.pivot(index='item_code', columns='customer', values='qty')
	res_t = df.pivot(index='customer', columns='item_code', values='qty')
	res_t = res_t.fillna(0)
	
	# console(str(df)).log()
	# console(str(df_t)).log()
	# console(str(res_t)).log()
	# console(str(res['Test'].to_dict())).log()
	# console(str(res.to_dict())).log()
	
	
	
	# data_dict = df.groupby(["customer"])["item_code","qty"].apply(lambda x: x.to_dict(orient="index")).to_dict()
	
	if flag == True:	
		columns, data  = get_columns(filters,res.to_dict().keys()), get_data(filters,res_t)
		
	else:
		columns, data = get_columns(filters,res.to_dict().keys()),""

	return columns, data

def get_customer_groupwise_data(filters=None):
	from_date = getdate(filters.get("from_date"))
	to_date = getdate(filters.get("to_date"))

	value = {"from_date": str(from_date), "to_date": str(to_date)}
	
	data_sql = frappe.db.sql("""
	select cs.customer_group, items.item_code, SUM(items.planned_qty) as qty, items.creation from `tabProduction Plan Item` items inner join `tabCustomer` cs on items.customer = cs.name where items.customer != "" and items.creation BETWEEN %(from_date)s AND ADDDATE(%(to_date)s, 1) GROUP BY customer_group, items.item_code
	""", values=value , as_dict = 1)
	# data = [{'customer': 'Test', 'item_code': 'Item A', 'qty': 1.0}, {'customer': 'Test 1', 'item_code': 'Item B', 'qty': 1.0}, {'customer': 'Test 2', 'item_code': 'Item A', 'qty': 1.0}, {'customer': 'Test 3', 'item_code': 'Item A', 'qty': 1.0}, {'customer': 'Test 4', 'item_code': 'Item B', 'qty': 1.0}, {'customer': 'Test 5', 'item_code': 'Item B', 'qty': 1.0}, {'customer': 'Test 6', 'item_code': 'Item B', 'qty': 1.0}, {'customer': 'Test 7', 'item_code': 'Item B', 'qty': 1.0}, {'customer': 'Test 8', 'item_code': 'Item A', 'qty': 1.0}, {'customer': 'Test 9', 'item_code': 'Item A', 'qty': 12.0}, {'customer': 'Test 9', 'item_code': 'Item B', 'qty': 10.0}]
	
	data = []
	for d in data_sql:
		d_n = dict(d)
		data.append(d_n)

	# console('data:' + str(data)).log()
	flag = True
	if data == []:
		d = {"customer_group": "", "item_code": "", "qty": 0, "creation": ""}
		data.append(d)
		flag = False

	df = pd.DataFrame.from_dict(data)
	# df_t = df.transpose()
	res = df.pivot(index='item_code', columns='customer_group', values='qty')
	res_t = df.pivot(index='customer_group', columns='item_code', values='qty')
	res_t = res_t.fillna(0)
	
	# console(str(df)).log()
	# console(str(df_t)).log()
	# console(str(res_t)).log()
	# console(str(res['Test'].to_dict())).log()
	# console(str(res.to_dict())).log()
		
		
		
		# data_dict = df.groupby(["customer"])["item_code","qty"].apply(lambda x: x.to_dict(orient="index")).to_dict()

	if flag == True:	
		columns, data  = get_columns(filters,res.to_dict().keys()), get_data(filters,res_t)
		
	else:
		columns, data = get_columns(filters,res.to_dict().keys()),""
		
	return columns, data

def get_customer_territorypwise_data(filters=None):
	from_date = getdate(filters.get("from_date"))
	to_date = getdate(filters.get("to_date"))

	value = {"from_date": str(from_date), "to_date": str(to_date)}
	data_sql = frappe.db.sql("""
	select cs.territory, items.item_code, SUM(items.planned_qty) as qty, items.creation from `tabProduction Plan Item` items inner join `tabCustomer` cs on items.customer = cs.name where items.customer != "" and items.creation BETWEEN %(from_date)s AND ADDDATE(%(to_date)s, 1) GROUP BY territory, item_code
	""", values=value,as_dict = 1)
	
	flag = True
	data = []
	for d in data_sql:
		d_n = dict(d)
		data.append(d_n)

	if data == []:
		d = {"territory": "", "item_code": "", "qty": 0, "creation": ""}
		data.append(d)
		flag = False

	df = pd.DataFrame.from_dict(data)
	# df_t = df.transpose()
	res = df.pivot(index='item_code', columns='territory', values='qty')
	res_t = df.pivot(index='territory', columns='item_code', values='qty')
	res_t = res_t.fillna(0)
	
	# console(str(df)).log()
	# console(str(df_t)).log()
	# console(str(res_t)).log()
	# console(str(res['Test'].to_dict())).log()
	# console(str(res.to_dict())).log()
	
	
	
	# data_dict = df.groupby(["customer"])["item_code","qty"].apply(lambda x: x.to_dict(orient="index")).to_dict()
	
	if flag == True:	
		columns, data  = get_columns(filters,res.to_dict().keys()), get_data(filters,res_t)
		
	else:
		columns, data = get_columns(filters,res.to_dict().keys()),""

	return columns, data

def get_data(filters,item_data):
	data = []
	item_dict = item_data.to_dict()
	# console(str(item_dict)).log()
	for item_value in item_dict.keys():
		
		# console(str(item_value)).log()
		# console(str(item_dict.get(item_value))).log()
		data_dict = {}
		data_dict["item"] = item_value
		data_dict.update(item_dict.get(item_value))
		total_qty = 0
		for index, item in enumerate(data_dict.values()):
			if index != 0:
				total_qty = total_qty + float(item)

		available_qty = frappe.db.sql("""
		select actual_qty from `tabBin` where warehouse = 'Stores - A' and item_code = %s """,item_value)
		data_dict.update({
			"total_qty":total_qty,
			"available_qty":float(available_qty[0][0]),
			"short_qty":(float(available_qty[0][0]) - total_qty if float(available_qty[0][0]) < total_qty else 0) * -1
		})
		
		# console(available_qty[0][0]).log()
		data.append(data_dict)
	# console(str(data)).log()
	return data

def get_columns(filters,customers):
	
	
	columns = [
		
		{
			"label":_("Item"),
			"fieldname": "item",
			"fieldtype": "link",
			"options":"Item",
			"width": 90
		},		
	]

	for cust in customers:
		# frappe.msgprint(str(cust))
		columns.append({
			"label":_(cust),
			"fieldname": cust,
			"fieldtype": "data",
			
			"width": 150
		})
	
	columns.append({
		"label":_("Total Qty"),
		"fieldtype":"float",
		"fieldname": "total_qty",

		"width": 90
	})
	# columns = columns + [
	# 	{
	# 		"label":_("Total Qty"),
	# 		"fieldtype":"float",
	# 		"fieldname": "total_qty",

	# 		"width": 90
	# 	},
	# 	{
	# 		"label":_("Available Qty"),
	# 		"fieldtype":"float",
	# 		"fieldname": "available_qty",

	# 		"width": 120
	# 	},
	# 	{
	# 		"label":_("Required Qty"),
	# 		"fieldtype":"float",
	# 		"fieldname": "short_qty",

	# 		"width": 120
	# 	}
	# ]

	return columns
