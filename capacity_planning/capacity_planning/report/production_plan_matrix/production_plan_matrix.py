# Copyright (c) 2013, raaj@akhilaminc.com and contributors
# For license information, please see license.txt

import frappe
from frappe import _
import pandas as pd
import numpy as np
# from console import console
from frappe.utils import date_diff

def execute(filters=None):
	
	if filters.get("report_type") == "Customer Name":
		customer_attribute = "customer"
		
	if filters.get("report_type") == "Customer Group":
		customer_attribute = "customer_group"
		
	if filters.get("report_type") == "Customer Territory":
		customer_attribute = "territory"
	
	return get_customerdata(customer_attribute,filters)


def get_customerdata(customer_attribute,filters=None):
	
	if customer_attribute == "customer":
		column_name = "so.customer"
	if customer_attribute == "customer_group":
		column_name = "cs.customer_group"
	if customer_attribute == "territory":
		column_name = "cs.territory"
	
	data_sql = frappe.db.sql("""
	select {column_name},si.item_code,sum(si.stock_qty) as qty from `tabSales Order` so inner join `tabSales Order Item` si on si.parent = so.name inner join `tabCustomer` cs on so.customer = cs.name where so.docstatus = 1 and si.qty != si.delivered_qty and so.delivery_date between %s and %s and so.status in ("To Deliver and Bill","To Deliver") group by {column_name},si.item_code
	""".format(column_name = column_name),(filters.get("from_date"),filters.get("to_date")),as_dict = 1)
	
	# data = [{'customer': 'Test', 'item_code': 'Item A', 'qty': 1.0}, {'customer': 'Test 1', 'item_code': 'Item B', 'qty': 1.0}, {'customer': 'Test 2', 'item_code': 'Item A', 'qty': 1.0}, {'customer': 'Test 3', 'item_code': 'Item A', 'qty': 1.0}, {'customer': 'Test 4', 'item_code': 'Item B', 'qty': 1.0}, {'customer': 'Test 5', 'item_code': 'Item B', 'qty': 1.0}, {'customer': 'Test 6', 'item_code': 'Item B', 'qty': 1.0}, {'customer': 'Test 7', 'item_code': 'Item B', 'qty': 1.0}, {'customer': 'Test 8', 'item_code': 'Item A', 'qty': 1.0}, {'customer': 'Test 9', 'item_code': 'Item A', 'qty': 12.0}, {'customer': 'Test 9', 'item_code': 'Item B', 'qty': 10.0}]
	
	data = []
	for d in data_sql:
		d_n = dict(d)
		data.append(d_n)
	# frappe.msgprint(str(len(data)))
	if len(data) > 0:
		df = pd.DataFrame.from_dict(data)
		
		res = df.pivot(index='item_code', columns=customer_attribute, values='qty')
		
		res_t = df.pivot(index=customer_attribute, columns='item_code', values='qty')
		res_t = res_t.fillna(0)
		
			
		columns, data  = get_columns(filters,res.to_dict().keys()), get_data(filters,res_t)

		return columns, data
	else:
		return [],[]

def get_data(filters,item_data):
	data = []
	
	item_dict = item_data.to_dict()

	for item_value in item_dict.keys():
		
		data_dict = {}
		data_dict["item"] = item_value
		data_dict.update(item_dict.get(item_value))
		total_qty = 0
		for index, item in enumerate(data_dict.values()):
			if index != 0:
				total_qty = total_qty + float(item)


		available_qty = frappe.db.sql("""
		select actual_qty from `tabBin` where warehouse = 'Stores - A' and item_code = %s """,item_value)
		avail_qty = available_qty[0][0] if available_qty else 0

		man_days = date_diff(filters.get("to_date"),filters.get("from_date"))
		item_man_capacity = get_item_man_capacity(item_value,man_days)
		def_man_grp = frappe.db.get_value("Item",{"name":item_value},"manufacturing_group")
		per_day_capacity = frappe.db.get_value("Itemwise Capacity",{"item":item_value,"parent":def_man_grp},"capacity") if frappe.db.get_value("Itemwise Capacity",{"item":item_value,"parent":def_man_grp},"capacity") else 0
		short_qty = (float(avail_qty) - total_qty if float(avail_qty) < total_qty else 0) * -1
		data_dict.update({
			"total_qty":total_qty,
			"available_qty":float(avail_qty),
			"short_qty":short_qty,
			"man_cap":item_man_capacity,
			"man_grp":def_man_grp,
			"req_day":round(short_qty/per_day_capacity) if short_qty and per_day_capacity else 0
		})
		data.append(data_dict)
	return data

		
		

def get_item_man_capacity(item,days):
	man_cap_per_day = frappe.db.get_value("Manufacturing Group",{"item":item,"is_group":1},"capacity")
	if man_cap_per_day and days:
		return float(man_cap_per_day) * float(days)
	else:
		return 0

	# frappe.msgprint(str(item))
	# frappe.msgprint(str(days))
	


def get_columns(filters,customers):
	
	
	columns = [
		
		{
			"label":_("Item"),
			"fieldname": "item",
			"fieldtype": "link",
			"options":"Item",
			"width": 90
		},
		
	]

	for cust in customers:
		
		columns.append({
			"label":_(cust),
			"fieldname": cust,
			"fieldtype": "data",
			
			"width": 150
		})
		
	columns = columns + [
		{
			"label":_("Total Qty"),
			"fieldtype":"float",
			"fieldname": "total_qty",

			"width": 90
		},
		{
			"label":_("Available Qty"),
			"fieldtype":"float",
			"fieldname": "available_qty",

			"width": 120
		},
		{
			"label":_("Required Qty"),
			"fieldtype":"float",
			"fieldname": "short_qty",

			"width": 120
		}
		,
		{
			"label":_("Manufacturing Group"),
			"fieldtype":"Data",
			"fieldname": "man_grp",

			"width": 80
		},
		{
			"label":_("Expected Days to Complete"),
			"fieldtype":"float",
			"fieldname": "req_day",

			"width": 120
		},
		
		{
			"label":_("Manufacturing Capacity"),
			"fieldtype":"float",
			"fieldname": "man_cap",

			"width": 120
		}

	]

	return columns
