// Copyright (c) 2016, raaj@akhilaminc.com and contributors
// For license information, please see license.txt
/* eslint-disable */

frappe.query_reports["Production Plan Matrix"] = {
	"filters": [
		{
			"fieldname":"report_type",
			"label": __("Report Type"),
			"fieldtype": "Select",
			"width": "80",
			"options": ["Customer Group", "Customer Territory", "Customer Name"],
			"default": "Customer Group"
		},
		{
			"fieldname":"from_date",
			"label": __("From Date"),
			"fieldtype": "Date",
			"default": frappe.datetime.add_months(frappe.datetime.get_today(), -1),
			"reqd": 1,
			"width": "60px"
		},
		{
			"fieldname":"to_date",
			"label": __("To Date"),
			"fieldtype": "Date",
			"default": frappe.datetime.get_today(),
			"reqd": 1,
			"width": "60px"
		}
	]
};
