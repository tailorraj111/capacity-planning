from pickle import FALSE

from numpy import remainder
import frappe
from frappe.utils import getdate
from capacity_planning.capacity_planning.report.manufacturing_capacity_tracking.manufacturing_capacity_tracking import get_capacity


@frappe.whitelist()
def get_planned_qty(date,man_group,item_code):
    date = getdate(date)
    planned_data = frappe.db.sql("""
    select sum(planned_qty) as qty from `tabProduction Plan Item` pli inner join `tabProduction Plan` pp on pp.name = pli.parent where pp.docstatus = 1 and date(pli.planned_start_date) = %s and pli.manufacturing_group = %s and pli.item_code = %s group by pli.planned_start_date,pli.manufacturing_group,pli.item_code
    """,(date,man_group,item_code),as_dict = 1)
    prod_cap = get_child_group_capacity(item_code,man_group)
    return planned_data,prod_cap


@frappe.whitelist()
def get_planned_item_qty(item_code):
    # date = getdate(date)
    planned_data = frappe.db.sql("""
    select sum(pli.planned_qty) as qty from `tabProduction Plan Item` pli inner join `tabProduction Plan` pp on pp.name = pli.parent where pp.docstatus = 1 and pli.item_code = %s and pp.status in ('Submitted') group by pli.item_code,pli.customer
    """,(item_code),as_dict = 1)
    # frappe.msgprint(str(planned_data))
    return planned_data




@frappe.whitelist()
def get_child_group_capacity(item,grp):
	capacity = frappe.db.sql("""
	select ic.capacity from `tabManufacturing Group` mg inner join `tabItemwise Capacity` ic on mg.name = ic.parent where mg.is_group = 0 and ic.item = %s and mg.name = %s
	""",(item,grp), as_dict = True)
	if capacity:
		return float(capacity[0].capacity)
	else:
		return 0

@frappe.whitelist()
def manufecture_grp_filter(doctype, txt, searchfield, start, page_len, filters):
    parent = filters.get("parent")
    item = filters.get("item")
    return frappe.db.sql("""
    select mg.name from `tabManufacturing Group` mg inner join `tabItemwise Capacity` ic on ic.parent = mg.name where mg.is_group = 0 and mg.parent_manufacturing_group = %s and ic.item = %s
    """,(parent, item))

@frappe.whitelist()
def main_manufecture_grp_filter(doctype, txt, searchfield, start, page_len, filters):
    item = filters.get("item")
    return frappe.db.sql("""
    select mg.name from `tabManufacturing Group` mg inner join `tabItemwise Capacity` ic on ic.parent = mg.name where mg.is_group = 1 and ic.item = %s
    """, item)

def validate(doc,method):
    planned_details = []
    for item in doc.po_items:
        if not any(d['manufacturing_group'] == item.manufacturing_group and d['date'] == getdate(item.planned_start_date) and d['item'] == item.item_code for d in planned_details):
            # frappe.msgprint(str(item.manufacturing_group))
            planned_details.append({
                "manufacturing_group":item.manufacturing_group,
                "date":getdate(item.planned_start_date),
                "qty":item.planned_qty,
                "item":item.item_code
            })
        else:
            for data in planned_details:
                if data['manufacturing_group'] == item.manufacturing_group and data['date'] == getdate(item.planned_start_date) and data['item'] == item.item_code:
                    data['qty'] = data['qty'] + item.planned_qty

    # frappe.msgprint(str(planned_details))
    for item in planned_details:
        prod_capacity = get_child_group_capacity(item["item"],item["manufacturing_group"])
        if item["qty"] > prod_capacity:
            frappe.throw("Production of Item {} on date {} by Manufacturing Group {} is out of capacity".format(item["item"],item["date"],item["manufacturing_group"]))


def on_submit(doc, method):
    items = []
    for item in doc.po_items:
            if items != []:
                flag = False
                for x in items:
                    if x['item_code'] == item.item_code:
                        remaining_qty = x['planned_qty'] - item.planned_qty
                        x['planned_qty'] = remaining_qty
                        flag = True
                        frappe.db.set_value('Manufacturing Plan Item', item.manufacturing_item_ref, 'planned_qty', x['planned_qty'])
                
                if flag == False:
                    remaining_qty = item.ordered_qty_client - item.planned_qty
                    store_item = {'item_code': item.item_code, 'planned_qty': remaining_qty}
                    items.append(store_item)
                    frappe.db.set_value('Manufacturing Plan Item', item.manufacturing_item_ref, 'planned_qty', remaining_qty)
            else:
                remaining_qty = item.ordered_qty_client - item.planned_qty
                store_item = {'item_code': item.item_code, 'planned_qty': remaining_qty}
                items.append(store_item)
                frappe.db.set_value('Manufacturing Plan Item', item.manufacturing_item_ref, 'planned_qty', remaining_qty)
            
