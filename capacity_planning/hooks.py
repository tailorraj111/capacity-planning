from . import __version__ as app_version

app_name = "capacity_planning"
app_title = "Capacity Planning"
app_publisher = "raaj@akhilaminc.com"
app_description = "Capacity Planning"
app_icon = "octicon octicon-file-directory"
app_color = "grey"
app_email = "raaj@ahilaminc.com"
app_license = "MIT"

# Includes in <head>
# ------------------

# include js, css files in header of desk.html
# app_include_css = "/assets/capacity_planning/css/capacity_planning.css"
# app_include_js = "/assets/capacity_planning/js/capacity_planning.js"

# include js, css files in header of web template
# web_include_css = "/assets/capacity_planning/css/capacity_planning.css"
# web_include_js = "/assets/capacity_planning/js/capacity_planning.js"

# include custom scss in every website theme (without file extension ".scss")
# website_theme_scss = "capacity_planning/public/scss/website"

# include js, css files in header of web form
# webform_include_js = {"doctype": "public/js/doctype.js"}
# webform_include_css = {"doctype": "public/css/doctype.css"}

# include js in page
# page_js = {"page" : "public/js/file.js"}

# include js in doctype views
doctype_js = {
	"Production Plan" : "capacity_planning/custom_doctype/production_plan/production_plan.js",
	"Job Card" : "custom_script/job_card.js",
	"Employee" : "custom_script/employee.js"
	}
# doctype_list_js = {"doctype" : "public/js/doctype_list.js"}
# doctype_tree_js = {"doctype" : "public/js/doctype_tree.js"}
# doctype_calendar_js = {"doctype" : "public/js/doctype_calendar.js"}

# Home Pages
# ----------

# application home page (will override Website Settings)
# home_page = "login"

# website user home page (by Role)
# role_home_page = {
#	"Role": "home_page"
# }

# Generators
# ----------

# automatically create page for each record of this doctype
# website_generators = ["Web Page"]

# Installation
# ------------

# before_install = "capacity_planning.install.before_install"
# after_install = "capacity_planning.install.after_install"

# Desk Notifications
# ------------------
# See frappe.core.notifications.get_notification_config

# notification_config = "capacity_planning.notifications.get_notification_config"

# Permissions
# -----------
# Permissions evaluated in scripted ways

# permission_query_conditions = {
# 	"Event": "frappe.desk.doctype.event.event.get_permission_query_conditions",
# }
#
# has_permission = {
# 	"Event": "frappe.desk.doctype.event.event.has_permission",
# }

# DocType Class
# ---------------
# Override standard doctype classes

# override_doctype_class = {
# 	"ToDo": "custom_app.overrides.CustomToDo"
# }

# Document Events
# ---------------
# Hook on document methods and events

doc_events = {
	"Work Order": {
		"validate": "capacity_planning.capacity_planning.work_order.set_manufecture_grp",
	},
	"Job Card":{
		"validate": "capacity_planning.capacity_planning.work_order.set_manufecture_grp_jobcard"
	},
	"Production Plan": {
		"validate" : "capacity_planning.capacity_planning.production_planning.validate",
		"on_submit": "capacity_planning.capacity_planning.production_planning.on_submit"
	}
}

# Scheduled Tasks
# ---------------

# scheduler_events = {
# 	"all": [
# 		"capacity_planning.tasks.all"
# 	],
# 	"daily": [
# 		"capacity_planning.tasks.daily"
# 	],
# 	"hourly": [
# 		"capacity_planning.tasks.hourly"
# 	],
# 	"weekly": [
# 		"capacity_planning.tasks.weekly"
# 	]
# 	"monthly": [
# 		"capacity_planning.tasks.monthly"
# 	]
# }

# Testing
# -------

# before_tests = "capacity_planning.install.before_tests"

# Overriding Methods
# ------------------------------
#
# override_whitelisted_methods = {
# 	"frappe.desk.doctype.event.event.get_events": "capacity_planning.event.get_events"
# }
#
# each overriding function accepts a `data` argument;
# generated from the base implementation of the doctype dashboard,
# along with any modifications made in other Frappe apps
# override_doctype_dashboards = {
# 	"Task": "capacity_planning.task.get_dashboard_data"
# }

# exempt linked doctypes from being automatically cancelled
#
# auto_cancel_exempted_doctypes = ["Auto Repeat"]


# User Data Protection
# --------------------

user_data_fields = [
	{
		"doctype": "{doctype_1}",
		"filter_by": "{filter_by}",
		"redact_fields": ["{field_1}", "{field_2}"],
		"partial": 1,
	},
	{
		"doctype": "{doctype_2}",
		"filter_by": "{filter_by}",
		"partial": 1,
	},
	{
		"doctype": "{doctype_3}",
		"strict": False,
	},
	{
		"doctype": "{doctype_4}"
	}
]

# Authentication and authorization
# --------------------------------

# auth_hooks = [
# 	"capacity_planning.auth.validate"
# ]


fixtures = [
    {"dt": "Custom Field", "filters": [
        [
            "name", "in", [
                "Production Plan Item-past_planned",
				"Production Plan Item-split",
				"Production Plan Item-actual_pending_quantity",
				"Production Plan Item-already_planned",
				"Production Plan Item-work_order_reference",
				"Production Plan-consolidate_customer",
				"Production Plan Item-customer",
				"Production Plan Item-group_capacity_per_day",
				"Production Plan Sales Order-delivery_date",
				"Production Plan Item-manufacturing_group",
				"Employee-manufacturing_sub_group",
				"Job Card-manufacturing_child_group",
				"Job Card-manufacturing_parent_group",
				"Work Order-manufacturing_group",
            ]
        ]
    ]}
]
