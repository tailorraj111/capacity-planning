from frappe import _

def get_data():
	return [
		{
			"module_name": "Capacity Planning",
			"color": "grey",
			"icon": "octicon octicon-file-directory",
			"type": "module",
			"label": _("Capacity Planning")
		}
	]
