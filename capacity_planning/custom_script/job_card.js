frappe.ui.form.on('Job Card', {
	refresh(frm) {
			frm.set_query("manufacturing_child_group", function() {
            return {
                filters: {
                    "parent_manufacturing_group":frm.doc.manufacturing_parent_group,
                    "is_group": 0
                }
            }
        });
	}
})