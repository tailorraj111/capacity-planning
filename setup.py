from setuptools import setup, find_packages

with open('requirements.txt') as f:
	install_requires = f.read().strip().split('\n')

# get version from __version__ variable in capacity_planning/__init__.py
from capacity_planning import __version__ as version

setup(
	name='capacity_planning',
	version=version,
	description='Capacity Planning',
	author='raaj@akhilaminc.com',
	author_email='raaj@ahilaminc.com',
	packages=find_packages(),
	zip_safe=False,
	include_package_data=True,
	install_requires=install_requires
)
